package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/joof/test-web-server.git/todo"
)

func main() {
	router := mux.NewRouter().StrictSlash(true)

	todo.AddRoutesTo(router)

	log.Fatal(http.ListenAndServe(":8080", router))
}
