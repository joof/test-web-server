package todo

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"sort"
	"strconv"
)

//func Index(w http.ResponseWriter, r *http.Request) {
//	_, _ = fmt.Fprintln(w, "Welcome!")
//}

func todoList(w http.ResponseWriter, r *http.Request) {
	todos := make([]*Todo, 0, len(allTodos))
	for _, todo := range allTodos{
		todos = append(todos, todo)
	}
	sort.Slice(todos, func(i, j int) bool {
		return todos[i].ID < todos[j].ID
	})
	_ = json.NewEncoder(w).Encode(todos)
}

func todoCreate(w http.ResponseWriter, r *http.Request) {
	todo := new(Todo)
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	err := dec.Decode(todo)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	biggestId++
	todo.ID = biggestId
	allTodos[todo.ID] = todo
	//w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	_ = json.NewEncoder(w).Encode(*todo)
}

func todoRetrieve(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	todoId, err := strconv.Atoi(vars["todo-id"])
	if err != nil {
		http.NotFound(w, r)
		return
	}
	todo, ok := allTodos[todoId]
	if !ok{
		http.NotFound(w, r)
		return
	}
	_ = json.NewEncoder(w).Encode(*todo)
}

func todoPartialUpdate(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	todoId, err := strconv.Atoi(vars["todo-id"])
	if err != nil {
		http.NotFound(w, r)
		return
	}
	todo, ok := allTodos[todoId]
	if !ok{
		http.NotFound(w, r)
		return
	}
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	err = dec.Decode(todo)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	allTodos[todoId] = todo
	_ = json.NewEncoder(w).Encode(*todo)
}

func todoUpdate(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	todoId, err := strconv.Atoi(vars["todo-id"])
	if err != nil {
		http.NotFound(w, r)
		return
	}
	_, ok := allTodos[todoId]
	if !ok{
		http.NotFound(w, r)
		return
	}
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	newTodo := &Todo{}
	err = dec.Decode(newTodo)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	allTodos[todoId] = newTodo
	_ = json.NewEncoder(w).Encode(*newTodo)
}

func todoDelete(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	todoId, err := strconv.Atoi(vars["todo-id"])
	if err != nil {
		http.NotFound(w, r)
		return
	}
	if _, ok := allTodos[todoId]; !ok {
		http.NotFound(w, r)
		return
	}
	delete(allTodos, todoId)
	w.WriteHeader(http.StatusNoContent)
}