package todo

import "time"


type Todo struct {
	ID		  int		`json:"id"`
	Name      string 	`json:"name"`
	Completed bool 		`json:"completed"`
	Due       time.Time `json:"due"`
}

var biggestId int = 4

var allTodos = map[int]*Todo{
	1: {ID: 1, Name: "Write presentation"},
	2: {ID: 2, Name: "Host meetup"},
	3: {ID: 3, Name: "Darband meetup"},
	4: {ID: 4, Name: "Eshqal meetup"},
}