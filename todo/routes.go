package todo

import "github.com/gorilla/mux"


func AddRoutesTo(router *mux.Router) {
	router.HandleFunc("/todos/", todoList).Methods("GET").Name("todo-list")
	router.HandleFunc("/todos/", todoCreate).Methods("POST").Name("todo-create")
	router.HandleFunc("/todos/{todo-id}/", todoRetrieve).Methods("GET").Name("todo-retrieve")
	router.HandleFunc("/todos/{todo-id}/", todoPartialUpdate).Methods("PATCH").Name("todo-partial-update")
	router.HandleFunc("/todos/{todo-id}/", todoUpdate).Methods("PUT").Name("todo-update")
	router.HandleFunc("/todos/{todo-id}/", todoDelete).Methods("DELETE").Name("todo-delete")
}