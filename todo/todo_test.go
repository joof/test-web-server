package todo

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func setupServer(inputFunc func(w http.ResponseWriter, r *http.Request)) *httptest.Server{
	srv := httptest.NewServer(http.HandlerFunc(inputFunc))
	return srv
}

//func TestTodoBasicList(t *testing.T) {
//	router := mux.NewRouter().StrictSlash(true)
//	AddRoutesTo(router)
//	_ = http.ListenAndServe(":8080", router)
//
//	request, _ := http.NewRequest(http.MethodGet, "/players/Pepper", nil)
//	response := httptest.NewRecorder()
//
//
//	got := response.Body.S
//	t.Run("returns Pepper's score", func(t *testing.T) {
//		request, _ := http.NewRequest(http.MethodGet, "/players/Pepper", nil)
//		response := httptest.NewRecorder()
//
//		PlayerServer(response, request)
//
//		got := response.Body.String()
//		want := "20"
//
//		if got != want {
//			t.Errorf("got %q, want %q", got, want)
//		}
//	})
//}

type expectTable struct {
	status int
	body   string
}

type resultTable struct {
	input  string
	expect expectTable
}

func TestTodoPost(t *testing.T) {
	server := setupServer(todoCreate)
	defer server.Close()

	myTable := []resultTable{
		{input: `{"name":"my-test-request-body"}`,
			expect: expectTable{status: 201,
				body: fmt.Sprintf(`{"id":%d,"name":"my-test-request-body","completed":false,"due":"0001-01-01T00:00:00Z"}`, biggestId+1) + "\n"},
		},
		{input: `{"name":"my-test2-request-body", "completed":false}`,
			expect: expectTable{status: 201,
				body: fmt.Sprintf(`{"id":%d,"name":"my-test2-request-body","completed":false,"due":"0001-01-01T00:00:00Z"}`, biggestId+2) + "\n"}},
		{input: `{"name":"my-test3-request-body", "completed":true, "due":"0001-01-01T00:00:00Z"}`,
			expect: expectTable{status: 201,
				body: fmt.Sprintf(`{"id":%d,"name":"my-test3-request-body","completed":true,"due":"0001-01-01T00:00:00Z"}`, biggestId+3) + "\n"},
		},
		{input: `{"name":"my-test4-request-body", "completed":true, "due":"1891-12-12T18:05:02Z"}`,
			expect: expectTable{status: 201,
				body: fmt.Sprintf(`{"id":%d,"name":"my-test4-request-body","completed":true,"due":"1891-12-12T18:05:02Z"}`, biggestId+4) + "\n"},
		},
		{input: ``,
			expect: expectTable{status: 400}},
	}

	for _, test := range myTable {
		reader := ioutil.NopCloser(strings.NewReader(test.input))
		req, _ := http.NewRequest("POST", "/todo/", reader)
		req.Header.Set("Content-Type", "application/json")
		w := httptest.NewRecorder()
		todoCreate(w, req)
		resp := w.Result()
		assert.Equal(t, test.expect.status, resp.StatusCode)
		if resp.StatusCode != 201 {
			continue
		}
		buf := new(strings.Builder)
		_, err := io.Copy(buf, resp.Body)
		assert.Nil(t, err, nil)
		body := buf.String()
		// check errors
		assert.Equal(t, test.expect.body, body)
		resp.Body.Close()
	}
}
